#!/bin/sh
# Outputs an escaped password for mutt
pass "$1" | sed -e 's/["$`\\]/\\&/g' -e 's/^/set imap_pass = "/' -e 's/$/"/'
